const {Schema, model} = require('mongoose');

const userSchema = new Schema(
    {
      email: {
        type: String,
        required: true,
      },
      role: {
        type: String,
        enum: ['SHIPPER', 'DRIVER'],
        required: true,
      },
      password: {
        type: String,
        required: true,
      },
    },
    {timestamps: {createdAt: 'created_date'}},
);

module.exports = model('User', userSchema);
