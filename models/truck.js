const {Schema, model} = require('mongoose');
const {TRUCK_TYPES} = require('../controllers/helpers/constants/truck');

const truckSchema = new Schema(
    {
      created_by: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
      },
      assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      type: {
        type: String,
        enum: TRUCK_TYPES,
        required: true,
      },
      status: {
        type: String,
        enum: ['OL', 'IS'],
        required: true,
      },
    },

    {timestamps: {createdAt: 'created_date'}},
);

module.exports = model('Truck', truckSchema);
