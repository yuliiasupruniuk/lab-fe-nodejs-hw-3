const {Schema, model} = require('mongoose');
const {STATE, STATUS} = require('../controllers/helpers/constants/load');

const loadSchema = new Schema(
    {
      created_by: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
      },
      assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      status: {
        type: String,
        enum: STATUS,
        required: true,
      },
      state: {
        type: String,
        enum: STATE,
      },
      name: {
        type: String,
        required: true,
      },
      payload: {
        type: Number,
        required: true,
      },
      pickup_address: {
        type: String,
        required: true,
      },
      delivery_address: {
        type: String,
        required: true,
      },
      dimensions: {
        width: {
          type: Number,
          required: true,
        },
        length: {
          type: Number,
          required: true,
        },
        height: {
          type: Number,
          required: true,
        },
      },
      logs: [
        {
          message: {
            type: String,
          },
          time: {
            type: String,
          },
        },
      ],
    },
    {timestamps: {createdAt: 'created_date'}},
);

module.exports = model('Load', loadSchema);
