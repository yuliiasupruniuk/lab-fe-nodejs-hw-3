const Router = require('express');
const router = new Router();

const LoadController = require('../controllers/loadController');
const authMiddleware = require('../middleware/authMiddleware');
const logMiddleware = require('../middleware/logMiddleware');
const roleMiddleware = require('../middleware/roleMiddleware');
const idMiddleware = require('../middleware/idMiddleware');
const {
  loadCreationValidation,
} = require('../middleware/validationMiddleware');


router.get('/', [authMiddleware, logMiddleware], LoadController.getAllLoads);

router.get(
    '/active',
    [authMiddleware, roleMiddleware.isDriver, logMiddleware],
    LoadController.getActiveLoad);

router.get(
    '/:id',
    [authMiddleware, idMiddleware, logMiddleware],
    LoadController.getLoadById);

router.get(
    '/:id/shipping_info',
    [authMiddleware, roleMiddleware.isShipper, idMiddleware, logMiddleware],
    LoadController.getShippingInfo);

router.post(
    '/',
    [authMiddleware, roleMiddleware.isShipper,
      loadCreationValidation, logMiddleware],
    LoadController.addLoad);

router.put(
    '/:id',
    [authMiddleware, roleMiddleware.isShipper, idMiddleware, logMiddleware],
    LoadController.updateLoad);

router.post(
    '/:id/post',
    [authMiddleware, roleMiddleware.isShipper, idMiddleware, logMiddleware],
    LoadController.postLoad);

router.patch(
    '/active/state',
    [authMiddleware, roleMiddleware.isDriver, logMiddleware],
    LoadController.nextLoadState);

router.delete(
    '/:id',
    [authMiddleware, roleMiddleware.isShipper, idMiddleware, logMiddleware],
    LoadController.deleteLoad);

module.exports = router;
