const Router = require('express');
const router = new Router();

const authRouter = require('./auth');
const userRouter = require('./users');
const truckRouter = require('./trucks');
const loadRouter = require('./loads');

router.use('/auth', authRouter);
router.use('/users/me', userRouter);
router.use('/trucks', truckRouter);
router.use('/loads', loadRouter);

module.exports = router;
