const Router = require('express');
const router = new Router();

const UserController = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');
const logMiddleware = require('../middleware/logMiddleware');
const {
  changePasswordValidation,
} = require('../middleware/validationMiddleware');

router.get('/', [authMiddleware, logMiddleware], UserController.getUserProfile);

router.patch(
    '/password',
    [authMiddleware, changePasswordValidation, logMiddleware],
    UserController.updateUserProfile);

router.delete(
    '/',
    [authMiddleware, logMiddleware],
    UserController.deleteUserProfile);

module.exports = router;
