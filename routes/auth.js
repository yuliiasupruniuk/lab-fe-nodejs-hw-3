const Router = require('express');
const router = new Router();

const AuthController = require('../controllers/authController');

const {
  registrationValidation,
  loginValidation,
} = require('../middleware/validationMiddleware');

router.post('/login', loginValidation, AuthController.login);
router.post('/register', registrationValidation, AuthController.register);

module.exports = router;
