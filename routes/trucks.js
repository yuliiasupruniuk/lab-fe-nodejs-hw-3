const Router = require('express');
const router = new Router();

const TruckController = require('../controllers/truckController');
const authMiddleware = require('../middleware/authMiddleware');
const logMiddleware = require('../middleware/logMiddleware');
const roleMiddleware = require('../middleware/roleMiddleware');
const idMiddleware = require('../middleware/idMiddleware');
const {
  truckCreationValidation,
} = require('../middleware/validationMiddleware');

router.get(
    '/',
    [authMiddleware, roleMiddleware.isDriver, logMiddleware],
    TruckController.getAllTrucks);

router.get(
    '/:id',
    [authMiddleware, roleMiddleware.isDriver, idMiddleware, logMiddleware],
    TruckController.getTruckById);

router.post(
    '/',
    [authMiddleware, roleMiddleware.isDriver,
      truckCreationValidation, logMiddleware],
    TruckController.addTruck);

router.post(
    '/:id/assign',
    [authMiddleware, roleMiddleware.isDriver, idMiddleware, logMiddleware],
    TruckController.assignTruck);

router.put(
    '/:id',
    [authMiddleware, roleMiddleware.isDriver, idMiddleware, logMiddleware],
    TruckController.updateTruck);

router.delete(
    '/:id',
    [authMiddleware, roleMiddleware.isDriver, idMiddleware, logMiddleware],
    TruckController.deleteTruck);

module.exports = router;
