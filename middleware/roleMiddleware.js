const ApiError = require('../errors/apiError');

/**
 * Check if user is driver
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function isDriver(req, res, next) {
  if (req.user.role !== 'DRIVER') {
    return next(
        ApiError.badRequest('Only drivers have permition for this route'));
  }
  next();
}

/**
 * Check if user is driver
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function isShipper(req, res, next) {
  if (req.user.role !== 'SHIPPER') {
    return next(
        ApiError.badRequest('Only shippers have permition for this route'));
  }
  next();
}

module.exports = {isDriver, isShipper};
