const jwt = require('jsonwebtoken');
require('dotenv').config();
const JWT_KEY = process.env.JWT_KEY;
const ApiError = require('../errors/apiError');
const User = require('../models/user');

module.exports = async function(req, res, next) {
  if (req.method == 'OPTIONS') {
    next();
  }
  try {
    const token = req.headers.authorization.split(' ')[1];

    if (!token) {
      return next(ApiError.badRequest(`Not Authorized`));
    }

    const decoded = jwt.verify(token, JWT_KEY);
    req.user = decoded;

    const user = await User.findOne({_id: decoded.id});
    if (!user) {
      return next(ApiError.badRequest(`User not found`));
    }

    next();
  } catch (error) {
    return next(ApiError.badRequest(`Not Authorized`));
  }
};
