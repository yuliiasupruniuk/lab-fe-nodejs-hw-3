const ApiError = require('../errors/apiError');
const joi = require('joi');
const {ROLES} = require('../controllers/helpers/constants/user');
const {TRUCK_TYPES} = require('../controllers/helpers/constants/truck');

/**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function registrationValidation(req, res, next) {
  const schema = joi.object({
    email: joi.string().email().required(),
    password: joi.string().required(),
    role: joi
        .string()
        .valid(...ROLES)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return next(ApiError.badRequest(`Registration field validation failed`));
  }
}

/**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function loginValidation(req, res, next) {
  const schema = joi.object({
    email: joi.string().email().required(),
    password: joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return next(ApiError.badRequest(`Login field validation failed`));
  }
}

/**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function changePasswordValidation(req, res, next) {
  const schema = joi.object({
    oldPassword: joi.string().required(),
    newPassword: joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return next(
        ApiError.badRequest(`Changing password field validation failed`));
  }
}

/**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function loadCreationValidation(req, res, next) {
  const schema = joi.object({
    name: joi.string().required(),
    payload: joi.number().required(),
    pickup_address: joi.string().required(),
    delivery_address: joi.string().required(),
    dimensions: joi.object({
      width: joi.number().required(),
      height: joi.number().required(),
      length: joi.number().required()})});

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return next(
        ApiError.badRequest(`Load creation field validation failed`));
  }
}

/**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
async function truckCreationValidation(req, res, next) {
  const schema = joi.object({
    type: joi
        .string()
        .valid(...TRUCK_TYPES)
        .required()});

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return next(
        ApiError.badRequest(`Truck creation field validation failed`));
  }
}

module.exports = {
  registrationValidation,
  changePasswordValidation,
  loginValidation,
  loadCreationValidation,
  truckCreationValidation,
};
