const ApiError = require('../errors/apiError');
const Load = require('../models/load');
const Truck = require('../models/truck');
const {STATUS} = require('./helpers/constants/load');
const {getSuitableTruckTypes} = require('./helpers/truckTypes');
const {getNextState} = require('./helpers/loadStates');

/**
   * Handles load requests
   */
class LoadController {
  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getAllLoads(req, res, next) {
    const userRole = req.user.role;

    try {
      if (userRole === 'SHIPPER') {
        new LoadController().getShipperLoads(req, res, next);
      } else if (userRole === 'DRIVER') {
        new LoadController().getDriverLoads(req, res, next);
      }
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getShipperLoads(req, res, next) {
    const userId = req.user.id;

    let {status, limit, offset} = req.query;
    offset = +offset || 0;
    limit = +limit || 10;

    if (limit > 50) {
      return next(ApiError.badRequest(`The limit should not be more than 50`));
    }

    try {
      let loads = [];
      if (!status || STATUS.indexOf(status) === -1) {
        loads = await Load.find(
            {created_by: userId},
            {updatedAt: 0, __v: 0})
            .skip(offset)
            .limit(limit);
      } else {
        loads = await Load.find(
            {created_by: userId, status},
            {updatedAt: 0, __v: 0})
            .skip(offset)
            .limit(limit);
      }

      res.json({loads});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getDriverLoads(req, res, next) {
    const userId = req.user.id;

    let {status, limit, offset} = req.query;
    offset = +offset || 0;
    limit = +limit || 10;

    if (limit > 50) {
      return next(ApiError.badRequest(`The limit should not be more than 50`));
    }

    try {
      let loads = [];
      if (!status) {
        loads = await Load.find(
            {assigned_to: userId},
            {updatedAt: 0, __v: 0})
            .skip(offset)
            .limit(limit);
      } else if (['ASSIGNED', 'SHIPPED'].indexOf(status) === -1) {
        return next(ApiError.badRequest(`Unavailable status`));
      } else {
        loads = await Load.find(
            {assigned_to: userId, status},
            {updatedAt: 0, __v: 0})
            .skip(offset)
            .limit(limit);
      }

      res.json({loads});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getActiveLoad(req, res, next) {
    const userId = req.user.id;

    try {
      const load = await Load.findOne({
        assigned_to: userId,
        status: 'ASSIGNED',
      });

      if (!load) {
        return next(ApiError.badRequest(`No active load found`));
      }
      res.json({load});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getLoadById(req, res, next) {
    const userId = req.user.id;
    const loadId = req.params.id;

    try {
      const load = await Load.findOne(
          {_id: loadId, created_by: userId},
          {updatedAt: 0, __v: 0});

      if (!load) {
        return next(ApiError.badRequest(`Load with id ${loadId} not found`));
      }

      res.json({load});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getShippingInfo(req, res, next) {
    const userId = req.user.id;
    const loadId = req.params.id;

    try {
      const load = await Load.findOne(
          {_id: loadId, created_by: userId},
          {updatedAt: 0, __v: 0});

      if (!load) {
        return next(ApiError.badRequest(`Load with id ${loadId} not found`));
      }

      if (load.status === 'NEW' || load.status === 'POSTED') {
        return next(ApiError.badRequest(`No exist loads shipping`));
      }

      const truck = await Truck.findOne(
          {assigned_to: load.assigned_to},
          {updatedAt: 0, __v: 0});

      res.json({load, truck});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async addLoad(req, res, next) {
    const userId = req.user.id;
    const {name, payload, dimensions} = req.body;

    try {
      const load = new Load({
        created_by: userId,
        status: 'NEW',
        name,
        payload,
        pickup_address: req.body.pickup_address,
        delivery_address: req.body.delivery_address,
        dimensions,
      });

      await load.save();

      res.json({
        message: 'Load created successfully',
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async postLoad(req, res, next) {
    const userId = req.user.id;
    const loadId = req.params.id;

    try {
      const load = await Load.findOne(
          {_id: loadId, created_by: userId, status: 'NEW'},
          {updatedAt: 0, __v: 0});

      if (!load) {
        return next(ApiError.badRequest(`Load with id ${loadId} not found`));
      }

      await load.updateOne({
        $set: {
          status: 'POSTED',
        },
        $push: {
          logs: {
            message: `Load status changed to 'POSTED'`,
            time: new Date(Date.now()).toISOString(),
          },
        },
      });

      let driverFound = false;
      const truckTypes = getSuitableTruckTypes(load.dimensions, load.payload);

      if (truckTypes.length !== 0) {
        const truck = await Truck.findOne({
          status: 'IS',
          assigned_to: {$ne: null},
          type: {$in: truckTypes},
        });

        if (truck) {
          await truck.updateOne({
            status: 'OL',
          });

          await load.updateOne({
            $set: {
              status: 'ASSIGNED',
              state: 'En route to Pick Up',
              assigned_to: truck.assigned_to,
            },
            $push: {
              logs: {
                message: `Load assigned to driver with id ${truck.assigned_to}`,
                time: new Date(Date.now()).toISOString(),
              },
            },
          });

          driverFound = true;
        }
      }

      if (!driverFound) {
        await load.updateOne({
          $set: {
            status: 'NEW',
          },
          $push: {
            logs: {
              message: 'Driver not found',
              time: new Date(Date.now()).toISOString(),
            },
          },
        });
      }

      res.json({message: 'Load posted successfully',
        driver_found: driverFound});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async updateLoad(req, res, next) {
    const userId = req.user.id;
    const loadId = req.params.id;
    const data = req.body;

    try {
      const load = await Load.findOne({_id: loadId, created_by: userId});

      if (!load) {
        return next(ApiError.badRequest(`Load with id ${loadId} not found`));
      }

      if (load.status !== 'NEW') {
        throw next(
            ApiError.badRequest(
                `You can update info only about loads with status 'NEW'`));
      }

      await load.updateOne({$set: data});
      res.json({message: 'Load details changed successfully'});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async nextLoadState(req, res, next) {
    const userId = req.user.id;

    try {
      const load = await Load.findOne({
        assigned_to: userId,
        status: 'ASSIGNED',
      });

      if (!load) {
        return next(ApiError.badRequest(`No active load found`));
      }

      const state = getNextState(load.state);

      if (state === 'Arrived to delivery') {
        await load.updateOne({
          $set: {
            state,
            status: 'SHIPPED',
          },
          $push: {
            logs: {
              message: `Load state is changed to ${state}`,
              time: new Date(Date.now()).toISOString(),
            },
          },
        });

        await Truck.findOneAndUpdate(
            {created_by: load.assigned_to, status: 'OL'},
            {
              $set: {
                status: 'IS',
              },
            });
      } else {
        await load.updateOne({
          $set: {
            state,
          },
          $push: {
            logs: {
              message: `Load state is changed to ${state}`,
              time: new Date(Date.now()).toISOString(),
            },
          },
        });
      }

      res.json({message: `Load state changed to ${state}`});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async deleteLoad(req, res, next) {
    const userId = req.user.id;
    const loadId = req.params.id;

    try {
      const load = await Load.findOne(
          {_id: loadId, created_by: userId},
          {updatedAt: 0, __v: 0});

      if (!load) {
        return next(ApiError.badRequest(`Load with id ${loadId} not found`));
      }

      if (load.status !== 'NEW') {
        return next(ApiError.badRequest(`You can delete only new loads`));
      }

      await Load.deleteOne({_id: loadId});

      res.json({message: 'Load deleted successfully'});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async deleteAllUserLoads(req, res, next) {
    const userId = req.user.id;

    try {
      await Load.deleteMany({created_by: userId});
      return;
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }
}

module.exports = new LoadController();
