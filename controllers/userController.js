const ApiError = require('../errors/apiError');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const LoadController = require('../controllers/loadController');
const TruckController = require('../controllers/truckController');

/**
 * User profile handler
 */
class UserController {
  /**
   * User profile view
   * @param {object} req - request
   * @param {object} res - response
   */
  async getUserProfile(req, res) {
    const {id, email, role} = req.user;

    res.json({
      user: {
        _id: id,
        email,
        role,
        created_date: req.user.created_date,
      },
    });
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async updateUserProfile(req, res, next) {
    const {oldPassword, newPassword} = req.body;
    const userId = req.user.id;

    try {
      const user = await User.findOne({_id: userId});

      const comparePassword = await bcrypt.compare(oldPassword, user.password);
      if (!comparePassword) {
        return next(ApiError.badRequest('Wrong password'));
      }

      const passwordHash = await bcrypt.hash(newPassword, 7);

      await user.updateOne({
        $set: {
          password: passwordHash,
        },
      });

      res.json({
        message: 'Password changed successfully',
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async deleteUserProfile(req, res, next) {
    const userId = req.user.id;

    try {
      await User.deleteOne({_id: userId});
      if (req.user.role === 'DRIVER') {
        await TruckController.deleteAllUserTrucks(req, res, next);
      } else {
        await LoadController.deleteAllUserLoads(req, res, next);
      }

      res.json({
        message: 'Profile deleted successfully',
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }
}

module.exports = new UserController();
