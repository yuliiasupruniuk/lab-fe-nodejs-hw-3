const ApiError = require('../errors/apiError');
const Truck = require('../models/truck');
const {TRUCK_TYPES} = require('./helpers/constants/truck');

/**
   * Handles truck requests
   */
class TruckController {
/**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getAllTrucks(req, res, next) {
    const userId = req.user.id;
    let trucks = [];
    try {
      trucks = await Truck.find(
          {created_by: userId},
          {updatedAt: 0, __v: 0});

      res.json({trucks});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async getTruckById(req, res, next) {
    const userId = req.user.id;
    const truckId = req.params.id;

    try {
      const truck = await Truck.findOne(
          {_id: truckId, created_by: userId},
          {updatedAt: 0, __v: 0});

      if (!truck) {
        return next(ApiError.badRequest(`Truck with id ${truckId} not found`));
      }

      res.json({truck});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }


  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async addTruck(req, res, next) {
    const userId = req.user.id;
    const type = req.body.type;

    try {
      const truck = new Truck({type, created_by: userId, status: 'IS'});
      await truck.save();

      res.json({
        message: 'Truck created successfully',
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async updateTruck(req, res, next) {
    const userId = req.user.id;
    const truckId = req.params.id;
    const type = req.body.type;

    if (!type) {
      return next(ApiError.badRequest(`Field 'type' is required`));
    }

    try {
      const truck = await Truck.findOne(
          {_id: truckId, created_by: userId},
          {updatedAt: 0, __v: 0});

      if (!truck) {
        return next(ApiError.badRequest(`Truck with id ${truckId} not found`));
      }

      if (truck.status === 'OL') {
        return next(
            ApiError.badRequest(
                `You are not able to change truck info while it is on a load`));
      }

      if (truck.assigned_to === userId) {
        return next(
            ApiError.badRequest(
                `You are not able to change truck info' + 
                ' while it is assigned on you`));
      }

      if (TRUCK_TYPES.indexOf(type) === -1) {
        return next(ApiError.badRequest(`Unknown truck type`));
      }

      await truck.updateOne({
        $set: {
          type,
        },
      });

      res.json({
        message: `Truck details changed successfully`,
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async assignTruck(req, res, next) {
    const userId = req.user.id;
    const truckId = req.params.id;

    try {
      const truck = await Truck.findOne({_id: truckId, created_by: userId});

      if (!truck) {
        return next(
            ApiError.internal(`You don't have truck with id ${truckId}`));
      }

      if (truck.status === 'OL') {
        return next(
            ApiError.badRequest(
                `You are not able to change truck info while it is on a load`));
      }

      const isAlreadyAssigned = await Truck.findOne({
        _id: truckId,
        assigned_to: userId,
      });

      if (isAlreadyAssigned) {
        return next(ApiError.badRequest(`Truck is already assigned`));
      }

      const assignedTrucks = await Truck.find({assigned_to: userId});
      if (assignedTrucks.length > 0) {
        return next(ApiError.badRequest(`User already has assignment`));
      }

      await truck.updateOne({
        $set: {
          assigned_to: userId,
        },
      });
      res.json({message: 'Truck assigned successfully'});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async deleteTruck(req, res, next) {
    const userId = req.user.id;
    const truckId = req.params.id;

    try {
      const truck = await Truck.findOne(
          {_id: truckId, created_by: userId},
          {updatedAt: 0, __v: 0});

      if (!truck) {
        return next(ApiError.badRequest(`Truck with id ${truckId} not found`));
      }

      if (truck.status === 'OL' || truck.assigned_to === userId) {
        return next(
            ApiError.badRequest(
                `You are not able to change truck info while it is on a load`));
      }

      await Truck.deleteOne({_id: truckId});

      res.json({message: 'Truck deleted successfully'});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
 * @param {object} req - request
 * @param {object} res - response
 * @param {function} next
 */
  async deleteAllUserTrucks(req, res, next) {
    const userId = req.user.id;

    try {
      await Truck.deleteMany({created_by: userId});
      return;
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }
}

module.exports = new TruckController();
