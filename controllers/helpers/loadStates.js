const {STATE} = require('./constants/load');

/**
 * Helps to find next load state
 * @param {string} currentState
 * @return {string}
 */
function getNextState(currentState) {
  const id = STATE.indexOf(currentState);
  if (STATE[id + 1]) {
    return STATE[id + 1];
  }
}

module.exports = {getNextState};
