const TYPES = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

/**
 * Truck handler
 */
class Truck {
  /**
   * @param {number} width
   * @param {number} length
   * @param {number} height
   * @param {number} payload
   */
  constructor(width, length, height, payload) {
    this.width = width;
    this.length = length;
    this.height = height;
    this.payload = payload;
  }

  /**
   * @param {object} dimensions
   * @param {number} payload
   * @return {boolean}
   */
  checkSizes(dimensions, payload) {
    if (
      dimensions.width < this.width &&
      dimensions.height < this.height &&
      dimensions.length < this.length &&
      payload < this.payload
    ) {
      return true;
    }
    return false;
  }
}

/**
 * Sprinter truck type with sizes
 */
class Sprinter extends Truck {
  /**
 * Creates instance for this model
 */
  constructor() {
    super(300, 250, 170, 1700);
  }
}


/**
 * Small Straight truck type with sizes
 */
class SmallStraight extends Truck {
  /**
 * Creates instance for this model
 */
  constructor() {
    super(500, 250, 170, 2500);
  }
}

/**
 * Large Straight truck type with sizes
 */
class LargeStraight extends Truck {
  /**
 * Creates instance for this model
 */
  constructor() {
    super(700, 350, 200, 4000);
  }
}

/**
 * @param {string} type
 * @return {object}
 */
function getTruckByType(type) {
  switch (type) {
    case 'SPRINTER':
      return new Sprinter();
    case 'SMALL STRAIGHT':
      return new SmallStraight();
    case 'LARGE STRAIGHT':
      return new LargeStraight();
    default:
      break;
  }
}

/**
 * @param {object} dimensions
 * @param {number} payload
 * @return {array}
 */
function getSuitableTruckTypes(dimensions, payload) {
  const suitableTypes = [];
  for (const truck of TYPES) {
    const TruckType = getTruckByType(truck);
    if (TruckType.checkSizes(dimensions, payload)) {
      suitableTypes.push(truck);
    }
  }

  return suitableTypes;
}

module.exports = {getSuitableTruckTypes};
