# LAB FE NodeJS HW 3
This is an imitation of Uber. Application was created for the practice of doing Node.js servers. For authorization and data storage, was used MongoDB.


## How to run?
 Please make sure you have installed node and npm in your system.
 ```
 node -v
 npm -v
 ```
 
 After checking if you have Node installed in your system, you can start app:
 ```
 git clone https://gitlab.com/yuliiasupruniuk/lab-fe-nodejs-hw-3.git
 cd lab-fe-nodejs-hw-3
 npm install
 npm start
